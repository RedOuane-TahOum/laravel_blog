<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Post;

class PostsController extends Controller
{
       public function __construct()
    {
        $this->middleware('auth',['except'=>['index','show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Post::orderBy('created_at','desc')->paginate(3);
        return view('pages.posts')->with('posts',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
                        'title'=>'required',
                        'content'=>'required',
                        'cover' => 'image|nullable|max:1999'
                                 ]
                        );
                if($request->hasFile('cover')){
           $UploadedFile = $request->file('cover')->getClientOriginalName();
           $FileName = pathinfo($UploadedFile,PATHINFO_FILENAME);
           $FileExtension = $request->file('cover')->getClientOriginalExtension();
           $Cover_img = $FileName.'_'.time().'.'.$FileExtension;
           $path = $request->file('cover')->storeAs('public/covers',$Cover_img);
          
           
           }
        
        $post = new Post;
        $post->title = $request->input('title');
        $post->content = $request->input('content');
        $post->cover = $Cover_img;
        $post->user_id = auth()->user()->id;
        $post->save();
        return redirect('/posts')->with('success','post created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $post = Post::find($id);
        return view('pages.show')->with('post',$post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
           $this->validate($request,
                                   [
                                      'title'=>'required',
                                      'content'=>'required',
                                      'cover' => 'image|nullable|max:1999'
                                   ]
                          );
                    if($request->hasFile('cover'))
                    {
                            $UploadedFile = $request->file('cover')->getClientOriginalName();
                            $FileName = pathinfo($UploadedFile,PATHINFO_FILENAME);
                            $FileExtension = $request->file('cover')->getClientOriginalExtension();
                            $Cover_img = $FileName.'_'.time().'.'.$FileExtension;
                            $path = $request->file('cover')->storeAs('public/covers',$Cover_img); 
                     }
 
        $post = Post::find($id);
        $post->title = $request->input('title');
        $post->content = $request->input('content');
       if($request->hasFile('cover'))
       {
              $post->cover=   $Cover_img;            
       }
        $post->save();
        return redirect('/posts')->with('success','post updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          $post = Post::find($id);
          if(auth()->user()->id != $post->user_id){
            return redirect('/posts')->with('error','post "'.$post->title. '" dosent belong to you');
          }
          if($post->cover!='DefaultCover.jpg'){
              Storage::delete('public/covers/'.$post->cover);
              
          }
          
          $post->delete();
            return redirect('/posts')->with('success','post "'.$post->title. '" deleted');
    }
}
