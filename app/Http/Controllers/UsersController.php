<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;

class UsersController extends Controller
{
           public function __construct()
    {
        $this->middleware('auth');
    }
    public function account(){
        
        return view('account.account',array('user'=>Auth::user()));
    }
    public function store(Request $request){
        if($request->hasFile('image')){
           $image = $request->file('image');
           $filename = time().'.'.$image->getClientOriginalExtension();
           Image::make($image)->resize(250,250)->save(public_path('storage/covers/'.$filename));
           $user = Auth::user();
           $user->image = $filename;
          $user->save();
        }
        return view('account.account',array('user'=>Auth::user()));
        
    }
}
