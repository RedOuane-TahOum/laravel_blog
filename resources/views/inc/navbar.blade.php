    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div   class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="https://laravel.com/"> <img src="http://chromemedia.com/wp-content/uploads/2014/01/laravel-1.png" width="40" height="30" alt=""></a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="/">Home</a></li>
            
						<li><a href="/posts">Blog</a></li>
          </ul>
        <ul class="nav navbar-nav navbar-right">
				@if(Auth::guest())
						<li><a href="{{ route('register') }}"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>	
						<li ><a href="{{ route('login') }}"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
				@else
							     <li class="dropdown">
                                <a style="position:relative; padding-left:50px;" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
																    <img style="width:35px;height:35px; position:absolute; top:10px; left:10px; border-radius:50%;" src="/storage/covers/{{Auth::user()->image}}">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
																    <li>
																			  <a href="/account" ><span class="glyphicon glyphicon-user"></span> Account</a>
																		</li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                          <span class="glyphicon glyphicon-log-out"></span>  Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
																		
                                </ul>
                            </li>
        @endif
				</ul>
        
     
    </div>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
        