<div style="padding-top:35px" class="container">
    <div class="row">
       <div class="col-md-2">
        
       </div>
         <div class="col-md-8">
                    @if(count($errors) > 0)
                      @foreach($errors->all() as $error)
                          <div class="alert alert-danger">
                          {{$error}}
                          </div>
                      @endforeach
                    @endif
                
                    @if(session('success'))
                        <div class="alert alert-success">
                            {{session('success')}}
                        </div>  
                    @endif
                
                    @if(session('error'))
                        <div class="alert alert-error">
                            {{session('error')}}
                        </div>  
                    @endif
       </div>
         <div class="col-md-2">
        
       </div>
        
    </div>
</div>

