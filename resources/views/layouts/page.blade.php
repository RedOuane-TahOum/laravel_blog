<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Latest compiled and minified CSS -->
   <link rel="stylesheet" href="{{asset('css/app.css')}}">
      <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
  

    <title>{{ config('app.name', 'Laravel') }}</title>
  </head>
  <body> 
  @include('inc.navbar')
  </br>
  </br>
    
  @include('inc.validations')
  @yield('content')
     <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'article-ckeditor' );
    </script>
      
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> 
   
  </body>
</html>