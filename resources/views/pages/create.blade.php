@extends('layouts.page')
@section('content')

    <div class="container">
      <div class="row">
          <div  class="row-md-2">
            
          </div>
          <div  class="row-md-8">
            <h1>Create Post</h1>
                <hr>
                </br>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-2"></div>
                                <div class="col-md-8">
                                  {!! Form::open([
                                    'action'=>'PostsController@store',
                                    'method' => 'post',
                                    'enctype'=>'multipart/form-data'
                                  ]) !!}
                                    <div class="form-group">
                                      {{Form::label('title','Title')}}
                                      {{Form::text('title','',['class'=>'form-control','placeholder'=>'Post Title'])}}
                                      
                                    </div>
                                    <div class="form-group">
                                      {{Form::label('content','Content')}}
                                      {{Form::textarea('content','',['id'=>'article-ckeditor','class'=>'form-control','placeholder'=>'Post Content'])}}
                                      
                                    </div>
                                    <div class="form-group">
                                       {{Form::label('cover','Cover')}}
                                       {{Form::file('cover')}}
                                    </div>
                                      {{Form::submit('submit',['class'=>'btn btn-primary'])}}
                                  {!! Form::close() !!}                                                                         
                                </div>
                            <div class="col-md-2"></div>
                        </div>
                    </div>
            
          </div>
          <div  class="row-md-2">
            
          </div>
            <hr>
          <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; SIAD 2017</p>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </footer>
      </div>
        
    </div>    

@endsection