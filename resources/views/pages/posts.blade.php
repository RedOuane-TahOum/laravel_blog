

@extends('layouts.page')
@section('content')
    <!-- Page Content -->
    <div style="padding-top:40px;" class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">

                <h1 class="page-header">
                    Blog Posts
                    <small>Check it out</small>
                </h1>
            @if($posts->count() == 0)
            <h3>No posts found :(</h3>
            @endif
             @foreach($posts as $post)
                <!-- First Blog Post -->
                <h2>
                    <a href="/posts/{{$post->id}}">{{$post->title}}</a>
                </h2>
                <p class="lead">
                    by <a href="index.php">{{$post->user->name}}</a>
                </p>
                <p><span class="glyphicon glyphicon-time"></span> Posted on {{$post->created_at}}</p>
                <hr>
                <img width="900" height="300" class="img-responsive" src="/storage/covers/{{$post->cover}}" alt="">
                <hr>
                <p>{!!$post->content!!}</p>
                <a class="btn btn-primary" href="/posts/{{$post->id}}">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>

                <hr>
              @endforeach
              {{$posts->links()}}

             

            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div   class="col-md-4">

                <!-- Blog Search Well -->
                <div class="well">
                    <h4>Blog Search</h4>
                    <div class="input-group">
                        <input type="text" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <span class="glyphicon glyphicon-search"></span>
                        </button>
                        </span>
                    </div>
                    <!-- /.input-group -->
                </div>

                <!-- Blog Categories Well -->
                <div  class="well">
                    <h4>Blog Categories</h4>
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                            @foreach($posts as $post)
                                <li><a href="#">{{$post->title}}</a>
                                </li>
                            @endforeach
                            </ul>
                        </div>
                        <!-- /.col-lg-6 -->
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.col-lg-6 -->
                    </div>
                    <!-- /.row -->
                </div>

                <!-- Side Widget Well -->
                <div  class="well">
                    <h4>Side Widget Well</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
                </div>

            </div>

        </div>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; SIAD 2017</p>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->
@if(!Auth::guest())
      <form action="posts/create">    
<button type="submit"  data-toggle="tooltip" data-placement="top" title="Add Post!" style="
  position:fixed;
  bottom:10px;
  right:10px;
  margin:0;
  padding:5px 3px;
  width: 70px;
  height: 70px;
  padding: 10px 16px;
  font-size: 24px;
  line-height: 1.33;
  border-radius: 35px;"
  type="button" class="btn btn-default btn-circle btn-xl"><i class="glyphicon glyphicon-pencil"></i></button></form>
@endif

@endsection
