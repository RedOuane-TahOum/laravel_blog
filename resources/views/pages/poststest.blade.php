@extends('layouts.page')
@section('content')
   
        <div style="padding-top: 30px;" class="container">

                    @foreach($posts as $post)
                            <div class="well">
                            <div class="media">
                            <a class="pull-left" href="#">
                            <img class="media-object" src="http://placekitten.com/150/150">
                            </a>
                            <div class="media-body">
                            <h4 class="media-heading"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                            <p class="text-right">Author</p>
                            <p>{{$post->content}}</p>
                            <ul class="list-inline list-unstyled">
                            <li><span><i class="glyphicon glyphicon-calendar"></i> {{$post->created_at}} </span></li>
                            <li>|</li>
                            <span><i class="glyphicon glyphicon-comment"></i> 2 comments</span>
                            <li>|</li>
                            <li>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star-empty"></span>
                            </li>
                            <li>|</li>
                            <li>
                               <!-- Use Font Awesome http://fortawesome.github.io/Font-Awesome/ -->
                            <span><i class="fa fa-facebook-square"></i></span>
                            <span><i class="fa fa-twitter-square"></i></span>
                            <span><i class="fa fa-google-plus-square"></i></span>
                            </li>
                            </ul>
                            </div>
                            </div>
                            </div>
                    
              
                @endforeach
                {{$posts->links()}}
         
        </div>
  <form action="posts/create">    
<button type="submit"  data-toggle="tooltip" data-placement="top" title="Add Post!" style="
  position:fixed;
  bottom:10px;
  right:10px;
  margin:0;
  padding:5px 3px;
  width: 70px;
  height: 70px;
  padding: 10px 16px;
  font-size: 24px;
  line-height: 1.33;
  border-radius: 35px;"
  type="button" class="btn btn-default btn-circle btn-xl"><i class="glyphicon glyphicon-pencil"></i></button></form>
@endsection