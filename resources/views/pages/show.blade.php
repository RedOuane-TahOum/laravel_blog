@extends('layouts.page')
@section('content')
    </br>
    <div class="container">
        <div class="row">
           
            <div class="col-md-8">
               <h1 class="display-2">{{$post->title}}</h1>
               <hr>
                <img class="img-responsive" src="/storage/covers/{{$post->cover}}" alt="">
               <small>created at {{$post->created_at}} by {{$post->user->name}}</small>
               </br>
               </br>
               <p style="padding:10px 0px 15px 0px; ">{!!$post->content!!}</p>
                @if(!Auth::guest())
                @if(auth()->user()->id ==$post->user_id) 
                <hr>
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal">Edit</button>
                @endif
                @endif    
                                        <!-- Modal -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Update Post</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                            <div class="modal-body">
                                    {!! Form::open([
                                    'action'=>['PostsController@update', $post->id],
                                    'method' => 'post',
                                    'enctype'=>'multipart/form-data'
                                  ]) !!}
                                    <div class="form-group">
                                      {{Form::label('title','Title')}}
                                      {{Form::text('title',$post->title,['class'=>'form-control','placeholder'=>'Post Title'])}}
                                      
                                    </div>
                                    <div class="form-group">
                                      {{Form::label('content','Content')}}
                                      {{Form::textarea('content',$post->content,['id'=>'article-ckeditor','class'=>'form-control','placeholder'=>'Post Content'])}}
                                      {{Form::hidden('_method','PUT')}}
                                    </div>
                                    <div class="form-group">
                                      {{Form::label('cover','Cover')}}
                                      {{Form::file('cover')}}
                                    </div>    
                                        <div class="modal-footer">
                                      {{Form::submit('Save changes',['class'=>'btn btn-primary'])}}
                                        </div>
                                  {!! Form::close() !!} 
                            </div>
                        
                        </div>
                      </div>
                    </div>
                        @if(!Auth::guest())
                         @if(auth()->user()->id ==$post->user_id)   
            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal2">Delete</button>
                         @endif
                        @endif                    <!-- Modal -->
                            <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                     <h3>Delete "{{$post->title}}"?</h3>
                                  </div>
                                  <div class="modal-footer">
                                     {!!Form::open(['action'=>['PostsController@destroy',$post->id],'method'=>'post'])!!}
                                    {{Form::hidden('_method','delete')}}
                                    {{Form::submit('Delete',['class'=>'btn btn-primary'])}}
                                     {!!Form::close()!!}
                                   
                                    
                                  </div>
                                </div>
                              </div>
                            </div>
                
                        
                
            </div>
            <div style="padding-top:80px;" class="col-md-4">
                      <!-- Blog Search Well -->
                <div class="well">
                    <h4>Blog Search</h4>
                    <div class="input-group">
                        <input type="text" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <span class="glyphicon glyphicon-search"></span>
                        </button>
                        </span>
                    </div>
                    <!-- /.input-group -->
                </div>

                <!-- Blog Categories Well -->
                <div class="well">
                    <h4>Blog Categories</h4>
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.col-lg-6 -->
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.col-lg-6 -->
                    </div>
                    <!-- /.row -->
                </div>

                <!-- Side Widget Well -->
                <div class="well">
                    <h4>Side Widget Well</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
                </div>
                
            </div>
        
        </div>
    <hr>
          <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; SIAD 2017</p>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </footer>
        
    </div>
@if(!Auth::guest())
      <form action="/posts/create">    
<button type="submit"  data-toggle="tooltip" data-placement="top" title="Add Post!" style="
  position:fixed;
  bottom:10px;
  right:10px;
  margin:0;
  padding:5px 3px;
  width: 70px;
  height: 70px;
  padding: 10px 16px;
  font-size: 24px;
  line-height: 1.33;
  border-radius: 35px;"
  type="button" class="btn btn-default btn-circle btn-xl"><i class="glyphicon glyphicon-pencil"></i></button></form>
@endif    
   
    
    
    
    
    
    
@endsection